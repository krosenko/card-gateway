﻿using System.Threading.Tasks;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;
using TransactionProcessor.Sagas.Data;

namespace TransactionProcessor.Sagas
{
    internal class ReverseTransactionSaga : Saga<ReverseTransactionData>,
        IAmStartedByMessages<ReverseTransaction>,
        IHandleMessages<ReversalSucceeded>,
        IHandleMessages<ReversalFailed>
    {
        private readonly ILogger<ReverseTransactionSaga> _logger;

        public ReverseTransactionSaga(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<ReverseTransactionSaga>();
        }

        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<ReverseTransactionData> mapper)
        {
            mapper.ConfigureMapping<ReverseTransaction>(message => message.TransactionId).ToSaga(data => data.TransactionId);
            mapper.ConfigureMapping<ReversalSucceeded>(message => message.TransactionId).ToSaga(data => data.TransactionId);
            mapper.ConfigureMapping<ReversalFailed>(message => message.TransactionId).ToSaga(data => data.TransactionId);
        }

        [Transaction]
        public async Task Handle(ReverseTransaction message, IMessageHandlerContext context)
        {
            _logger.LogDebug("Received {Command}", message);

            _logger.LogInformation("Starting reversal saga with {SagaData}.", Data);

            await SendReversal(context);
        }

        private async Task SendReversal(IMessageHandlerContext context)
        {
            if (Data.ReversalPending)
            {
                _logger.LogDebug("Reversal request is pending.");

                return;
            }

            if (Data.ReversalAttempt < 3)
            {
                Data.ReversalAttempt++;

                _logger.LogInformation("Requesting new reversal request {ReversalRequestCount}", Data.ReversalAttempt);

                var command = new SendReversal
                {
                    TransactionId = Data.TransactionId,
                };
                await context.Send(command);

                _logger.LogDebug("Sent {Command}", command);

                Data.ReversalPending = true;
            }
            else
            {
                _logger.LogInformation("Exceeded reversal request limit, giving up.");

                var @event = new TransactionReversalFailed
                {
                    TransactionId = Data.TransactionId,
                };
                await context.Publish(@event);

                _logger.LogDebug("Published {Event}", @event);

                _logger.LogDebug("Closing reversal saga.");

                MarkAsComplete();
            }
        }

        [Transaction]
        public async Task Handle(ReversalFailed message, IMessageHandlerContext context)
        {
            Data.ReversalPending = false;

            _logger.LogDebug("Reversal request failed.");

            await SendReversal(context);
        }

        [Transaction]
        public async Task Handle(ReversalSucceeded message, IMessageHandlerContext context)
        {
            Data.ReversalPending = false;

            _logger.LogDebug("Reversal request succeeded.");

            var @event = new TransactionReversed
            {
                TransactionId = Data.TransactionId,
            };
            await context.Publish(@event);

            _logger.LogDebug("Published {Event}", @event);

            _logger.LogDebug("Closing reversal saga.");

            MarkAsComplete();
        }
    }
}
