﻿using System.Threading.Tasks;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;
using TransactionProcessor.Sagas.Data;

namespace TransactionProcessor.Sagas
{
    internal class TransactionSaga : Saga<TransactionData>,
        IAmStartedByMessages<StartTransaction>,
        IHandleMessages<RequestSucceeded>,
        IHandleMessages<RequestFailed>,
        IHandleMessages<TransactionReversed>,
        IHandleMessages<TransactionReversalFailed>
    {
        private readonly ILogger<TransactionSaga> _logger;

        public TransactionSaga(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<TransactionSaga>();
        }

        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<TransactionData> mapper)
        {
            mapper.ConfigureMapping<StartTransaction>(message => message.TransactionId).ToSaga(data => data.TransactionId);
            mapper.ConfigureMapping<RequestSucceeded>(message => message.TransactionId).ToSaga(data => data.TransactionId);
            mapper.ConfigureMapping<RequestFailed>(message => message.TransactionId).ToSaga(data => data.TransactionId);
            mapper.ConfigureMapping<TransactionReversed>(message => message.TransactionId).ToSaga(data => data.TransactionId);
            mapper.ConfigureMapping<TransactionReversalFailed>(message => message.TransactionId).ToSaga(data => data.TransactionId);
        }

        [Transaction]
        public async Task Handle(StartTransaction message, IMessageHandlerContext context)
        {
            if (Data.ReverseStarted)
            {
                _logger.LogDebug("Transaction reversal already started, skipping send request.");

                return;
            }

            if (Data.RequestSent)
            {
                _logger.LogDebug("Transaction request already sent.");

                return;
            }

            _logger.LogInformation("Starting transaction saga.");

            await context.Send(new SendRequest
            {
                TransactionId = Data.TransactionId,
            });

            Data.RequestSent = true;
        }

        [Transaction]
        public Task Handle(RequestSucceeded message, IMessageHandlerContext context)
        {
            _logger.LogInformation("Request succeeded, saga is complete.");

            MarkAsComplete();

            return Task.CompletedTask;
        }

        [Transaction]
        public async Task Handle(RequestFailed message, IMessageHandlerContext context)
        {
            Data.RequestFailed = true;

            if (!Data.ReverseStarted)
            {
                _logger.LogInformation("Request failed, requesting transaction reversal.");

                await context.Send(new ReverseTransaction
                {
                    TransactionId = Data.TransactionId,
                });

                Data.ReverseStarted = true;
            }
        }

        [Transaction]
        public Task Handle(TransactionReversed message, IMessageHandlerContext context)
        {
            _logger.LogInformation("Transaction reversed, closing saga.");

            MarkAsComplete();

            return Task.CompletedTask;
        }

        [Transaction]
        public Task Handle(TransactionReversalFailed message, IMessageHandlerContext context)
        {
            _logger.LogInformation("Transaction reversal failed, giving up and closing saga.");

            MarkAsComplete();

            return Task.CompletedTask;
        }
    }
}
