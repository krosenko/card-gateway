﻿using System;
using NServiceBus;

namespace TransactionProcessor.Sagas.Data
{
    internal class ReverseTransactionData : ContainSagaData
    {
        public Guid TransactionId { get; set; }
        public int ReversalAttempt { get; set; }
        public bool ReversalPending { get; set; }
    }
}