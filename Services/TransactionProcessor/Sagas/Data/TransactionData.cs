﻿using System;
using NServiceBus;

namespace TransactionProcessor.Sagas.Data
{
    internal class TransactionData : ContainSagaData
    {
        public Guid TransactionId { get; set; }
        public bool RequestSent { get; set; }
        public bool RequestFailed { get; set; }
        public bool ReverseStarted { get; set; }
    }
}
