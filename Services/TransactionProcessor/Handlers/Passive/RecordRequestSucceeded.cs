﻿using System.Threading.Tasks;
using DataAccess;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;

namespace TransactionProcessor.Handlers.Passive
{
    internal class RecordRequestSucceeded : IHandleMessages<RequestSucceeded>
    {
        private readonly ILogger<RecordRequestSucceeded> _logger;
        private readonly TransactionStatusRepository _repository;

        public RecordRequestSucceeded(ILoggerFactory loggerFactory, TransactionStatusRepository repository)
        {
            _logger = loggerFactory.CreateLogger<RecordRequestSucceeded>();
            _repository = repository;
        }

        [Transaction]
        public async Task Handle(RequestSucceeded message, IMessageHandlerContext context)
        {
        }
    }
}
