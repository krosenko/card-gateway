﻿using System.Threading.Tasks;
using DataAccess;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;

namespace TransactionProcessor.Handlers.Passive
{
    internal class RecordReversalFailed : IHandleMessages<ReversalFailed>
    {
        private readonly ILogger<RecordReversalFailed> _logger;
        private readonly TransactionStatusRepository _repository;

        public RecordReversalFailed(ILoggerFactory loggerFactory, TransactionStatusRepository repository)
        {
            _logger = loggerFactory.CreateLogger<RecordReversalFailed>();
            _repository = repository;
        }

        [Transaction]
        public async Task Handle(ReversalFailed message, IMessageHandlerContext context)
        {
        }
    }
}