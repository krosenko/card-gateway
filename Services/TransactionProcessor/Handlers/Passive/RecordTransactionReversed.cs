﻿using System.Threading.Tasks;
using DataAccess;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;

namespace TransactionProcessor.Handlers.Passive
{
    internal class RecordTransactionReversed : IHandleMessages<TransactionReversed>
    {
        private readonly ILogger<RecordTransactionReversed> _logger;
        private readonly TransactionStatusRepository _repository;

        public RecordTransactionReversed(ILoggerFactory loggerFactory, TransactionStatusRepository repository)
        {
            _logger = loggerFactory.CreateLogger<RecordTransactionReversed>();
            _repository = repository;
        }

        [Transaction]
        public async Task Handle(TransactionReversed message, IMessageHandlerContext context)
        {
        }
    }
}