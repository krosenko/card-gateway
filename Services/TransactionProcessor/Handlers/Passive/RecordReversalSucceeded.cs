﻿using System.Threading.Tasks;
using DataAccess;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;

namespace TransactionProcessor.Handlers.Passive
{
    internal class RecordReversalSucceeded : IHandleMessages<ReversalSucceeded>
    {
        private readonly ILogger<RecordReversalSucceeded> _logger;
        private readonly TransactionStatusRepository _repository;

        public RecordReversalSucceeded(ILoggerFactory loggerFactory, TransactionStatusRepository repository)
        {
            _logger = loggerFactory.CreateLogger<RecordReversalSucceeded>();
            _repository = repository;
        }

        [Transaction]
        public async Task Handle(ReversalSucceeded message, IMessageHandlerContext context)
        {
        }
    }
}