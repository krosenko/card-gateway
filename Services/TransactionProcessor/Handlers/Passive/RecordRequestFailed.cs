﻿using System.Threading.Tasks;
using DataAccess;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;

namespace TransactionProcessor.Handlers.Passive
{
    internal class RecordRequestFailed : IHandleMessages<RequestFailed>
    {
        private readonly ILogger<RecordRequestFailed> _logger;
        private readonly TransactionStatusRepository _repository;

        public RecordRequestFailed(ILoggerFactory loggerFactory, TransactionStatusRepository repository)
        {
            _logger = loggerFactory.CreateLogger<RecordRequestFailed>();
            _repository = repository;
        }

        [Transaction]
        public async Task Handle(RequestFailed message, IMessageHandlerContext context)
        {
        }
    }
}