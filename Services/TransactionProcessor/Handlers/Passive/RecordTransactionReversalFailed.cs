﻿using System.Threading.Tasks;
using DataAccess;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;

namespace TransactionProcessor.Handlers.Passive
{
    internal class RecordTransactionReversalFailed : IHandleMessages<TransactionReversalFailed>
    {
        private readonly ILogger<RecordTransactionReversalFailed> _logger;
        private readonly TransactionStatusRepository _repository;

        public RecordTransactionReversalFailed(ILoggerFactory loggerFactory, TransactionStatusRepository repository)
        {
            _logger = loggerFactory.CreateLogger<RecordTransactionReversalFailed>();
            _repository = repository;
        }

        [Transaction]
        public async Task Handle(TransactionReversalFailed message, IMessageHandlerContext context)
        {
        }
    }
}