﻿using System;
using System.Threading.Tasks;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;

namespace TransactionProcessor.Handlers.Active
{
    internal class SendReversalHandler : IHandleMessages<SendReversal>
    {
        private readonly ILogger<SendReversalHandler> _logger;

        public SendReversalHandler(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<SendReversalHandler>();
        }

        [Transaction]
        public async Task Handle(SendReversal message, IMessageHandlerContext context)
        {
            _logger.LogDebug("Handling {Command}", message);

            try
            {
                _logger.LogInformation("Sending reversal request.");

                // send reversal

                _logger.LogDebug("Sent reversal request.");

                var @event = new ReversalSucceeded
                {
                    TransactionId = message.TransactionId,
                    Response = null,
                };
                await context.Publish(@event);

                _logger.LogDebug("Published {Event}", @event);
            }
            catch (Exception e)
            {
                _logger.LogInformation(e, "Failed to send reversal request.");

                var @event = new ReversalFailed
                {
                    TransactionId = message.TransactionId,
                    Response = e,
                };
                await context.Publish(@event);

                _logger.LogDebug("Published {Event}", @event);
            }
        }
    }
}