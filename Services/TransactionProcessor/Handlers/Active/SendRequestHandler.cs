﻿using System;
using System.Threading.Tasks;
using Messages;
using Microsoft.Extensions.Logging;
using NewRelic.Api.Agent;
using NServiceBus;

namespace TransactionProcessor.Handlers.Active
{
    internal class SendRequestHandler : IHandleMessages<SendRequest>
    {
        private readonly ILogger<SendRequestHandler> _logger;

        public SendRequestHandler(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<SendRequestHandler>();
        }

        [Transaction]
        public async Task Handle(SendRequest message, IMessageHandlerContext context)
        {
            _logger.LogDebug("Handling {Command}", message);

            // de-duplicate

            try
            {
                _logger.LogInformation("Sending request.");

                // send request

                _logger.LogDebug("Sent request.");

                var @event = new RequestSucceeded
                {
                    TransactionId = message.TransactionId,
                    Response = null,
                };
                await context.Publish(@event);

                _logger.LogDebug("Published {Event}", @event);
            }
            catch (Exception e)
            {
                _logger.LogInformation(e, "Failed to send request.");

                var @event = new RequestFailed
                {
                    TransactionId = message.TransactionId,
                    Response = e,
                };
                await context.Publish(@event);

                _logger.LogDebug("Published {Event}", @event);
            }
        }
    }
}
