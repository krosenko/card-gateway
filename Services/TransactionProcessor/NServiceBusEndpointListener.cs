﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Fabric;
using System.Threading;
using System.Threading.Tasks;
using DataAccess;
using Endpoints;
using Endpoints.Behaviors.Logging;
using Endpoints.Behaviors.Metrics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceFabric.Services.Communication.Runtime;
using NServiceBus;
using NServiceBus.Persistence.Sql;
using Serilog;
using Serilog.Extensions.Logging;
using Starbucks.Extensions.Configuration.ServiceFabric;

namespace TransactionProcessor
{
    public class NServiceBusEndpointListener : ICommunicationListener
    {
        private readonly IConfiguration _configuration;
        private readonly SqlConnectionStringBuilder _connectionStringBuilder;
        private readonly DbProviderFactory _dbProviderFactory;
        private readonly ILoggerFactory _loggerFactory;

        private IEndpointInstance _endpointInstance;

        public NServiceBusEndpointListener(ServiceContext serviceContext)
        {
            var configurationPackage = serviceContext.CodePackageActivationContext.GetConfigurationPackageObject("Config");
            _configuration = new ConfigurationRoot(new List<IConfigurationProvider>
            {
                new ServiceFabricConfigurationProvider(configurationPackage),
                new EnvironmentVariablesConfigurationProvider(),
            });
            _connectionStringBuilder = new SqlConnectionStringBuilder
            {
                ApplicationName = "CardGateway",
                ApplicationIntent = ApplicationIntent.ReadWrite,
                ConnectionString = _configuration.GetConnectionString("CardGatewayDatabase"),
            };
            _dbProviderFactory = DbProviderFactories.GetFactory(_configuration.GetConnectionString("CardGatewayDatabase_ProviderName"));

            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(_configuration)
                .CreateLogger();
            _loggerFactory = new LoggerFactory(new[]
            {
                new SerilogLoggerProvider(logger, true),
            });
            NServiceBus.Logging.LogManager.Use<MicrosoftLogFactory>().UseMsFactory(_loggerFactory);
        }

        private DbConnection DbConnectionBuilder()
        {
            var dbConnection = _dbProviderFactory.CreateConnection();
            if (dbConnection is null)
            {
                throw new ApplicationException("DbProviderFactory returned null.");
            }

            dbConnection.ConnectionString = _connectionStringBuilder.ConnectionString;
            return dbConnection;
        }

        private EndpointConfiguration CreateEndpointConfiguration(string endpointName)
        {
            var endpointConfiguration = new EndpointConfiguration(endpointName);
            endpointConfiguration.SetDiagnosticsPath(Environment.GetEnvironmentVariable("Fabric_Folder_App_Log"));
            endpointConfiguration.Pipeline.Register(builder => builder.Build<IncomingTransactionIdLogger>(), IncomingTransactionIdLogger.Description);
            endpointConfiguration.Pipeline.Register(builder => builder.Build<IncomingTransactionIdMetricEnricher>(), IncomingTransactionIdMetricEnricher.Description);
            endpointConfiguration.Pipeline.Register(builder => builder.Build<IncomingDistributedTraceCorrelator>(), IncomingDistributedTraceCorrelator.Description);
            endpointConfiguration.Pipeline.Register(builder => builder.Build<OutgoingDistributedTraceCorrelator>(), OutgoingDistributedTraceCorrelator.Description);
            endpointConfiguration.RegisterComponents(components =>
            {
                components.RegisterSingleton(_loggerFactory);
                components.ConfigureComponent(DbConnectionBuilder, DependencyLifecycle.InstancePerCall);
                components.ConfigureComponent<TransactionStatusRepository>(DependencyLifecycle.InstancePerCall);
                components.ConfigureComponent<IncomingTransactionIdLogger>(DependencyLifecycle.InstancePerCall);
                components.ConfigureComponent<IncomingTransactionIdMetricEnricher>(DependencyLifecycle.InstancePerCall);
                components.ConfigureComponent<IncomingDistributedTraceCorrelator>(DependencyLifecycle.InstancePerCall);
                components.ConfigureComponent<OutgoingDistributedTraceCorrelator>(DependencyLifecycle.InstancePerCall);
            });

            var serialization = endpointConfiguration.UseSerialization<NewtonsoftSerializer>();

            var persistence = endpointConfiguration.UsePersistence<SqlPersistence, StorageType.Sagas>();
//            var persistence = endpointConfiguration.UsePersistence<SqlPersistence>();
            persistence.ConnectionBuilder(DbConnectionBuilder);
            var dialect = persistence.SqlDialect<SqlDialect.MsSqlServer>();

            var transport = endpointConfiguration.UseAzureServiceBusTransport(_configuration.GetConnectionString("AzureServiceBus"));
            
            var routing = transport.Routing();
            routing.RouteTransactionMessages();

            return endpointConfiguration;
        }

        public async Task<string> OpenAsync(CancellationToken cancellationToken)
        {
            var endpointConfiguration = CreateEndpointConfiguration(NServiceBusEndpoints.TransactionProcessor);

            _endpointInstance = await Endpoint.Start(endpointConfiguration);

            return NServiceBusEndpoints.TransactionProcessor;
        }

        public async Task CloseAsync(CancellationToken cancellationToken)
        {
            if (!(_endpointInstance is null))
            {
                await _endpointInstance.Stop();
            }
        }

        public void Abort()
        {
            var _ = CloseAsync(CancellationToken.None);
        }
    }
}
