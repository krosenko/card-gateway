﻿using System;
using System.Threading.Tasks;
using Messages;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NServiceBus;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly ILogger<TransactionsController> _logger;
        private readonly IMessageSession _messageSession;

        public TransactionsController(ILogger<TransactionsController> logger, IMessageSession messageSession)
        {
            _logger = logger;
            _messageSession = messageSession;
        }

        [HttpGet("newGuid")]
        public ActionResult<Guid> GenerateGuid()
        {
            return Ok(Guid.NewGuid());
        }

        [HttpGet("{transactionId}")]
        public ActionResult<string> Get([FromRoute] Guid transactionId)
        {
            return "value";
        }

        [HttpPut("{transactionId}")]
        public async Task Put([FromRoute] Guid transactionId, [FromBody] string value)
        {
            var startTransaction = new StartTransaction
            {
                TransactionId = transactionId,
                Contents = value,
            };

            _logger.LogDebug("Sending {Command}", startTransaction);

            await _messageSession.Send(startTransaction);
        }

        [HttpDelete("{transactionId}")]
        public void Delete([FromRoute] Guid transactionId)
        {
            // do something
        }
    }
}