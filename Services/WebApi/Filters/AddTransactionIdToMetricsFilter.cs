﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApi.Filters
{
    public class AddTransactionIdToMetricsFilter : IAsyncActionFilter
    {
        public Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context.ActionArguments.TryGetValue("transactionId", out var transactionId)
                && transactionId is Guid guid)
            {
                NewRelic.Api.Agent.NewRelic.AddCustomParameter("TransactionId", guid.ToString("D"));
            }

            return next.Invoke();
        }
    }
}