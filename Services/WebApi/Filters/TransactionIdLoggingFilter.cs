﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace WebApi.Filters
{
    public class TransactionIdLoggingFilter : IAsyncActionFilter
    {
        private readonly ILogger<TransactionIdLoggingFilter> _logger;

        public TransactionIdLoggingFilter(ILogger<TransactionIdLoggingFilter> logger)
        {
            _logger = logger;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context.ActionArguments.TryGetValue("transactionId", out var transactionId)
                && transactionId is Guid guid)
            {
                using (_logger.BeginScope(new Dictionary<string, object>
                {
                    {"transactionId", guid},
                }))
                {
                    await next.Invoke();
                }
            }
            else
            {
                await next.Invoke();
            }
        }
    }
}
