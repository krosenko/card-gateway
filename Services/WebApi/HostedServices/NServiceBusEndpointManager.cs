﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using NServiceBus;

namespace WebApi.HostedServices
{
    /// <inheritdoc />
    /// <summary>
    /// A wrapper around an NServiceBus endpoint for the purpose of hooking up the lifecycle methods
    /// to the hosting container.
    /// </summary>
    /// <remarks>
    /// <para>By using a hosted service as a wrapper for the endpoint, we can guarantee that the web host
    /// will start the endpoint at the appropriate time, and clean up the endpoint on shutdown.</para>
    /// </remarks>
    public class NServiceBusEndpointManager : IHostedService
    {
        /// <summary>
        /// The currently-running endpoint.  This will be <c>null</c> before the service is started
        /// and after it is shut down.
        /// </summary>
        public IEndpointInstance Endpoint { get; private set; }

        private readonly EndpointConfiguration _endpointConfiguration;

        public NServiceBusEndpointManager(EndpointConfiguration endpointConfiguration)
        {
            _endpointConfiguration = endpointConfiguration;
        }

        /// <inheritdoc />
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (!(Endpoint is null))
            {
                throw new ApplicationException("Double initialization of the NServiceBus endpoint.");
            }

            Endpoint = await NServiceBus.Endpoint.Start(_endpointConfiguration);
        }

        /// <inheritdoc />
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await Endpoint.Stop();
            Endpoint = null;
        }
    }
}
