﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using Endpoints;
using Endpoints.Behaviors.Metrics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NServiceBus;
using Swashbuckle.AspNetCore.Swagger;
using WebApi.Filters;
using WebApi.HostedServices;

namespace WebApi
{
    public class Startup : StartupBase
    {
        private const string SwaggerDocumentName = "card-gateway";
        private const string SwaggerTitle = "Card Gateway";

        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            Configuration = configuration;

            NServiceBus.Logging.LogManager.Use<MicrosoftLogFactory>().UseMsFactory(loggerFactory);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<TransactionStatusRepository>();

            #region Database

            var connectionStringBuilder = new SqlConnectionStringBuilder
            {
                ApplicationName = "CardGateway",
                ApplicationIntent = ApplicationIntent.ReadWrite,
                ConnectionString = Configuration.GetConnectionString("CardGatewayDatabase"),
            };
            var dbProviderFactory = DbProviderFactories.GetFactory(Configuration.GetConnectionString("CardGatewayDatabase_ProviderName"));

            services.AddSingleton(dbProviderFactory);
            services.AddSingleton<DbConnectionStringBuilder>(connectionStringBuilder);
            services.AddTransient(DbConnectionBuilder);

            #endregion

            #region MVC

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddOptions<MvcOptions>().Configure(options =>
            {
                options.Filters.Add<TransactionIdLoggingFilter>();
                options.Filters.Add<AddTransactionIdToMetricsFilter>();
            });

            #endregion

            #region Proxy & Load Balancer
            
            services.AddOptions<ForwardedHeadersOptions>().Bind(Configuration.GetSection("ForwardedHeaders"));
            
            #endregion

            #region Swagger

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(SwaggerDocumentName, new Info()
                {
                    Title = SwaggerTitle,
                    Version = "1.0.0",
                    Description = "An experimental prototype for a semi-generic transaction service.",
                });
            });

            #endregion

            #region NServiceBus

            var endpointConfiguration = CreateEndpointConfiguration();
            services.AddSingleton(endpointConfiguration);
            services.AddHostedService<NServiceBusEndpointManager>();

            IEndpointInstance EndpointInstanceFactory(IServiceProvider provider)
            {
                return provider.GetRequiredService<NServiceBusEndpointManager>().Endpoint;
            }

            services.AddTransient<IEndpointInstance>(EndpointInstanceFactory);
            services.AddTransient<IMessageSession>(EndpointInstanceFactory);

            #endregion
        }

        private static DbConnection DbConnectionBuilder(IServiceProvider provider)
        {
            var connectionStringBuilder = provider.GetRequiredService<DbConnectionStringBuilder>();
            var dbProviderFactory = provider.GetRequiredService<DbProviderFactory>();

            var dbConnection = dbProviderFactory.CreateConnection();
            if (dbConnection is null)
            {
                throw new ApplicationException("DbProviderFactory returned null.");
            }

            dbConnection.ConnectionString = connectionStringBuilder.ConnectionString;
            return dbConnection;
        }

        private EndpointConfiguration CreateEndpointConfiguration()
        {
            var endpointConfiguration = new EndpointConfiguration(NServiceBusEndpoints.WebApi);
            endpointConfiguration.SendOnly();
            endpointConfiguration.SetDiagnosticsPath(Environment.GetEnvironmentVariable("Fabric_Folder_App_Log"));
            endpointConfiguration.Pipeline.Register(typeof(OutgoingDistributedTraceCorrelator), OutgoingDistributedTraceCorrelator.Description);

            var serialization = endpointConfiguration.UseSerialization<NewtonsoftSerializer>();

            var transport = endpointConfiguration.UseAzureServiceBusTransport(Configuration.GetConnectionString("AzureServiceBus"));

            var routing = transport.Routing();
            routing.RouteTransactionMessages();

            return endpointConfiguration;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void Configure(IApplicationBuilder app)
        {
            var env = app.ApplicationServices.GetRequiredService<IHostingEnvironment>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseForwardedHeaders();
            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.RoutePrefix = string.Empty;
                options.DisplayRequestDuration();
                options.SwaggerEndpoint($"/swagger/{SwaggerDocumentName}/swagger.json", SwaggerTitle);
            });
        }
    }
}
