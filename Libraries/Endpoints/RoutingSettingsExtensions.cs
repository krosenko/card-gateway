﻿using Messages;
using NServiceBus;

namespace Endpoints
{
    public static class RoutingSettingsExtensions
    {
        public static void RouteTransactionMessages(this RoutingSettings routing)
        {
            routing.RouteToEndpoint(typeof(StartTransaction).Assembly, NServiceBusEndpoints.TransactionProcessor);
        }
    }
}