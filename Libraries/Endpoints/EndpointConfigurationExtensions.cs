﻿using NServiceBus;

namespace Endpoints
{
    /// <summary>
    /// Extension methods for an NServiceBus endpoint configuration.
    /// </summary>
    public static class EndpointConfigurationExtensions
    {
        /// <summary>
        /// Sets up the endpoint to use Azure Service Bus as its transport.
        /// </summary>
        /// <param name="endpointConfiguration">The endpoint configuration to update.</param>
        /// <param name="connectionString">The connection string to Azure Service Bus.</param>
        /// <returns>The transport extensions for further customization.</returns>
        public static TransportExtensions<AzureServiceBusTransport> UseAzureServiceBusTransport(this EndpointConfiguration endpointConfiguration,
            string connectionString)
        {
            var transport = endpointConfiguration.UseTransport<AzureServiceBusTransport>();
            transport.ConnectionString(connectionString);
            return transport;
        }
    }
}
