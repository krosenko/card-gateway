﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Messages;
using Microsoft.Extensions.Logging;
using NServiceBus.Pipeline;

namespace Endpoints.Behaviors.Logging
{
    /// <inheritdoc />
    /// <summary>
    /// An NServiceBus behavior that attaches the transaction ID to all log messages
    /// generated while processing the message.
    /// </summary>
    public class IncomingTransactionIdLogger : Behavior<IIncomingLogicalMessageContext>
    {
        public static string Description { get; } = "Adds TransactionId to Log Messages.";

        private readonly ILogger<IncomingTransactionIdLogger> _logger;

        public IncomingTransactionIdLogger(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<IncomingTransactionIdLogger>();
        }

        /// <inheritdoc />
        public override async Task Invoke(IIncomingLogicalMessageContext context, Func<Task> next)
        {
            if (context.Message.Instance is IAmInATransaction inATransaction)
            {
                using (_logger.BeginScope(new Dictionary<string, object>
                {
                    {"TransactionId", inATransaction.TransactionId},
                }))
                {
                    await next();
                }
            }
            else
            {
                await next();
            }
        }
    }
}
