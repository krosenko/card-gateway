﻿using System;
using System.Threading.Tasks;
using NewRelic.Api.Agent;
using NServiceBus.Pipeline;

namespace Endpoints.Behaviors.Metrics
{
    /// <inheritdoc />
    /// <summary>
    /// An NServiceBus behavior that checks incoming messages for a NewRelic distributed trace
    /// payload, and if it finds one passes it to the current NewRelic transaction.
    /// </summary>
    public class IncomingDistributedTraceCorrelator : Behavior<IIncomingPhysicalMessageContext>
    {
        public static string Description { get; } = "Correlates incoming messages with distributed traces.";

        /// <inheritdoc />
        [Trace]
        public override async Task Invoke(IIncomingPhysicalMessageContext context, Func<Task> next)
        {
            if (context.MessageHeaders.TryGetValue(DistributedTraceConstants.DistributedTraceHeaderName, out var payload))
            {
                NewRelic.Api.Agent.NewRelic.GetAgent().CurrentTransaction?.AcceptDistributedTracePayload(payload, TransportType.Queue);
            }

            await next();
        }
    }
}
