﻿namespace Endpoints.Behaviors.Metrics
{
    /// <summary>
    /// Constants related to NewRelic distributed traces.
    /// </summary>
    public static class DistributedTraceConstants
    {
        /// <summary>
        /// The name of the header attached to NServiceBus messages that contains the NewRelic
        /// distributed trace payload.
        /// </summary>
        public const string DistributedTraceHeaderName = "DistributedTracePayload";
    }
}
