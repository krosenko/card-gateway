﻿using System;
using System.Threading.Tasks;
using NewRelic.Api.Agent;
using NServiceBus.Pipeline;

namespace Endpoints.Behaviors.Metrics
{
    /// <inheritdoc />
    /// <summary>
    /// An NServiceBus behavior that attaches a NewRelic distributed trace payload to outgoing messages.
    /// </summary>
    public class OutgoingDistributedTraceCorrelator : Behavior<IOutgoingPhysicalMessageContext>
    {
        public static string Description { get; } = "Decorates outgoing messages with distributed traces.";

        /// <inheritdoc />
        [Trace]
        public override async Task Invoke(IOutgoingPhysicalMessageContext context, Func<Task> next)
        {
            var currentTransaction = NewRelic.Api.Agent.NewRelic.GetAgent().CurrentTransaction;
            if (!(currentTransaction is null))
            {
                context.Headers.Add(DistributedTraceConstants.DistributedTraceHeaderName, currentTransaction.CreateDistributedTracePayload().Text());
            }

            await next();
        }
    }
}
