﻿using System;
using System.Threading.Tasks;
using Messages;
using NServiceBus.Pipeline;

namespace Endpoints.Behaviors.Metrics
{
    /// <inheritdoc />
    /// <summary>
    /// An NServiceBus behavior that checks if the incoming message is an <see cref="T:Messages.IAmInATransaction" />,
    /// and if so attaches the transaction ID to the NewRelic transaction.
    /// </summary>
    public class IncomingTransactionIdMetricEnricher : Behavior<IIncomingLogicalMessageContext>
    {
        public static string Description { get; } = "Adds TransactionId to metrics.";

        /// <inheritdoc />
        public override async Task Invoke(IIncomingLogicalMessageContext context, Func<Task> next)
        {
            if (context.Message.Instance is IAmInATransaction inATransaction)
            {
                NewRelic.Api.Agent.NewRelic.AddCustomParameter("TransactionId", inATransaction.TransactionId.ToString("D"));
            }

            await next();
        }
    }
}