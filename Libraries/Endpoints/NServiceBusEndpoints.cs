﻿namespace Endpoints
{
    public static class NServiceBusEndpoints
    {
        public static readonly string TransactionProcessor = "TransactionProcessor";
        public static readonly string WebApi = "WebApi";
    }
}
