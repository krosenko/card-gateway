﻿using System;
using System.Runtime.Serialization;
using NServiceBus;

namespace Messages
{
    [DataContract]
    public class SendReversal : ICommand, IAmInATransaction
    {
        [DataMember]
        public Guid TransactionId { get; set; }
    }
}