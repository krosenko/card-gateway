﻿using System;
using System.Runtime.Serialization;
using NServiceBus;

namespace Messages
{
    [DataContract]
    public class ReverseTransaction : ICommand, IAmInATransaction
    {
        [DataMember]
        public Guid TransactionId { get; set; }
    }
}