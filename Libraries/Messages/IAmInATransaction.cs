﻿using System;

namespace Messages
{
    /// <summary>
    /// Identifies a message as being part of a transaction saga and provides access to the transaction identifier.
    /// </summary>
    public interface IAmInATransaction
    {
        /// <summary>
        /// The transaction identifier.
        /// </summary>
        Guid TransactionId { get; }
    }
}
