﻿using System;
using System.Runtime.Serialization;
using NServiceBus;

namespace Messages
{
    [DataContract]
    public class TransactionReversed : IEvent, IAmInATransaction
    {
        [DataMember]
        public Guid TransactionId { get; set; }
    }
}