﻿using System;
using System.Runtime.Serialization;
using NServiceBus;

namespace Messages
{
    [DataContract]
    public class RequestSucceeded : IEvent, IAmInATransaction
    {
        [DataMember]
        public Guid TransactionId { get; set; }

        [DataMember]
        public object Response { get; set; }
    }
}