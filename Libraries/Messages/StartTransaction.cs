﻿using System;
using System.Runtime.Serialization;
using NServiceBus;

namespace Messages
{
    [DataContract]
    public class StartTransaction : ICommand, IAmInATransaction
    {
        [DataMember]
        public Guid TransactionId { get; set; }

        [DataMember]
        public object Contents { get; set; }
    }
}
