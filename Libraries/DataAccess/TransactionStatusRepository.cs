﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace DataAccess
{
    /// <summary>
    /// A repository accessor with methods to query and update the status of a transaction in the database.
    /// </summary>
    /// <remarks>
    /// <para>We need to track the results of NServiceBus sagas because results are communicated by events,
    /// and the only mechanism Particular Software recommends is to subscribe to those events and write them
    /// to a persistent store.  It is technically possible to get at the persisted saga data, but this is not
    /// a supported mechanism and may change at any time.</para>
    /// </remarks>
    public class TransactionStatusRepository
    {
        private readonly ILogger<TransactionStatusRepository> _logger;
        private readonly DbConnection _dbConnection;

        /// <summary>
        /// Creates a new repository accessor.
        /// </summary>
        /// <remarks>
        /// <para>This injects an <see cref="ILoggerFactory"/> instead of an <see cref="ILogger{TCategoryName}"/>
        /// directly in order to be compatible with the transaction processor, which uses NServiceBus's crippled
        /// dependency injection system.</para>
        /// <para>This injects a <see cref="DbConnection"/> instead of an <see cref="System.Data.IDbConnection"/>
        /// because the connection factory needed by NServiceBus must return the concrete type instead of the
        /// interface.  To be consistent, we use the same type in the web API as well.  As a bonus, this allows
        /// access to the <code>async</code> API.</para>
        /// </remarks>
        /// <param name="loggerFactory">A logger factory.</param>
        /// <param name="dbConnection">A database connection.</param>
        public TransactionStatusRepository(ILoggerFactory loggerFactory, DbConnection dbConnection)
        {
            _logger = loggerFactory.CreateLogger<TransactionStatusRepository>();
            _dbConnection = dbConnection;
        }

        public async Task<bool> CreateTransactionRecordAsync(Guid transactionId)
        {
            return true;
        }

        public async Task<object> GetTransactionStatusAsync(Guid transactionId)
        {
            return string.Empty;
        }
    }
}
